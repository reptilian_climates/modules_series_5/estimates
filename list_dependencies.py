

dependencies = [
	"OHF",
	"ships",
	"botanical",
	
	
	"semver",
	"toml",

	##
	#
	#

	"alpaca-py",

	"arrow",

	"backtrader",

	#
	#	https://basana.readthedocs.io/en/latest/quickstart.html
	#
	"basana",

	

	# https://github.com/santosjorge/cufflinks
	"cufflinks",

	"cryptography",

	"ccxt",

	"click",

	"dash",

	"finvizfinance",

	"ipychart",

	# http://lumibot.lumiwealth.com/getting_started.html#getting-started
	"lumibot",


	"matplotlib",
	"numpy",
	"pandas",
	"pandas_ta",
	"pandas_datareader",

	"plotly",
	"pydash",
	"PyJSONViewer",
	"PyJWT",
	"stock_indicators",
	"ta",

	# https://github.com/nardew/talipp
	"talipp",

	"technical_indicators_lib",

	#tensortrade

	"toml",

	"tradingview-ta",
	"tradingview-screener",

	#vectorbt==0.25.5 

	# https://pypi.org/project/yahooquery/
	"yahooquery",

	"yfinance"

]


import pkg_resources

def show_dependencies (module_name):
    # Load the distribution information for the specified module
    distribution = pkg_resources.get_distribution (module_name)

    # Get the list of required dependencies
    dependencies = distribution.requires()

    # Print the dependencies
    print(f"Dependencies of {module_name}:")
    for dependency in dependencies:
        print(dependency)


	
show_dependencies ("tradingview-ta")