

Bravo! You have received a Mercantilism Diploma in "rollercoaster" from the Orbital Convergence University International Air and Water Embassy of the Tangerine Planet (the planet that is one ellipse further from the Sun than Earth's ellipse).

You are now officially certified to include "rollercoaster" in your practice.

--
# rollercoaster

## install
```
pip install rollercoaster
```

## rides (OCHLV)
rides.season_3.super_hero_trend

## stats (options)
stats.aggregate_break_even
stats.aggregate_PC_ratio


## clouds
clouds.Coinbase.API.products.candles
