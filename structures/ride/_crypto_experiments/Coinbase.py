


def add_paths_to_system (paths):
	from os.path import dirname, join, normpath
	import pathlib
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'../../modules',
	'../../modules_pip'
])


import ccxt
import rollercoaster.clouds.CCXT.symbols.array as CCXT_symbols

symbols = CCXT_symbols.retrieve (
	exchange = ccxt.kraken ()
)




def galactic_ramp ():
	import pandas
	import rich	

	import ramps_galactic
	import ramps_galactic.victory_multiplier.purchase_treasure_at_inclines as purchase_treasure_at_inclines_VM	
	import ramps_galactic.victory_multiplier.purchase_treasure_over_span as purchase_treasure_over_span_VM
	import ramps_galactic.example_data.read as read_example_data

	'''
		Date,Open,High,Low,Close,Adj Close,Volume
	'''
	trend = read_example_data.start ("yahoo-finance--BTC-USD.CSV")	
	trend_DF = pandas.DataFrame (trend)	

	enhanced_trend_DF = ramps_galactic.calc (
		trend_DF,
		period = 14,
		multiplier = 2
	)
	enhanced_list = enhanced_trend_DF.to_dict ('records')


	'''
		This calculates the multipliers
	'''
	victory_multiplier_if_riding = purchase_treasure_at_inclines_VM.calc (
		enhanced_trend_DF,
		include_last_change = False
	)

	rich.print_json (data = victory_multiplier_if_riding ["relevant"])	

	victory_multiplier_if_holding = purchase_treasure_over_span_VM.calc (enhanced_trend_DF)
	print (
		"The multiplier if holding over the entire interval:",
		victory_multiplier_if_holding
	)
	print (
		"The multiplier if trading the ramp:", 
		victory_multiplier_if_riding ["treasure purchase victory multiplier"]
	)

	rich.print_json (data = victory_multiplier_if_riding)

	current_trend = victory_multiplier_if_riding ["current trend"]
	print ('current trend:', current_trend)


	def print_thoughts ():
		return;

	'''
		If you would like to open the chart:
	'''
	def show_chart ():
		ramps_galactic.chart_the_data (
			enhanced_trend_DF,
			victory_multiplier_if_riding
		)
		
		

	show_chart ()