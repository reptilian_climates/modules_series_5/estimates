



def add_paths_to_system (paths):
	from os.path import dirname, join, normpath
	import pathlib
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'../../../../modules',
	'../../../../modules_pip'
])

'''
	Find a stock that has undergone a stock split.
'''
import json
fp = open ("/online/crowns_safes/mint/alpaca.markets/paper.JSON", "r")
paper = json.loads (fp.read ())
fp.close ()

print (paper)


import requests





#url = "https://data.alpaca.markets/v2/stocks/bars?timeframe=1Min5Min5Min&limit=1000&adjustment=raw&feed=sip&sort=asc"

host = paper ['end point'] 
URL = f"{ host }/v2/stocks/bars"
URL = "https://data.alpaca.markets/v2/stocks/bars"

print (URL)

response = requests.get (
	URL,
	params = {
		'symbols': 'TAN',
		'timeframe': '5Min',
		
		#
		#	YYYY-MM-DD
		#
		'start': '2024-01-01',
		'end': '2024-03-17'
		
		#'limit': '1000',
		
	},
	headers = {
		"accept": "application/json",
		"APCA-API-KEY-ID": paper ['key'],
		"APCA-API-SECRET-KEY": paper ['secret']
	}
)

print(response.text)