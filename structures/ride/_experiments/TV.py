
def add_paths_to_system (paths):
	from os.path import dirname, join, normpath
	import pathlib
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'../../modules',
	'../../modules_pip'
])

import requests
URL = "https://scanner.tradingview.com/coin/scan"
data = {
	"columns": [
		"name",
		"description",
		"logoid",
		"update_mode",
		"type",
		"typespecs",
		"close",
		"pricescale",
		"minmov",
		"fractional",
		"minmove2",
		"currency",
		"change",
		"volume",
		"relative_volume_10d_calc",
		"market_cap_basic",
		"fundamental_currency_code",
		"price_earnings_ttm",
		"earnings_per_share_diluted_ttm",
		"earnings_per_share_diluted_yoy_growth_ttm",
		"dividends_yield_current",
		"sector.tr",
		"market",
		"sector",
		"recommendation_mark",
		"exchange"
	],
	"filter2": {
		"operands": [
			{
				"operation": {
					"operands": [
						{
							"operation": {
								"operands": [
									{
										"expression": {
											"left": "type",
											"operation": "equal",
											"right": "stock"
										}
									},
									{
										"expression": {
											"left": "typespecs",
											"operation": "has",
											"right": [
												"common"
											]
										}
									}
								],
								"operator": "and"
							}
						},
						{
							"operation": {
								"operands": [
									{
										"expression": {
											"left": "type",
											"operation": "equal",
											"right": "stock"
										}
									},
									{
										"expression": {
											"left": "typespecs",
											"operation": "has",
											"right": [
												"preferred"
											]
										}
									}
								],
								"operator": "and"
							}
						},
						{
							"operation": {
								"operands": [
									{
										"expression": {
											"left": "type",
											"operation": "equal",
											"right": "dr"
										}
									}
								],
								"operator": "and"
							}
						},
						{
							"operation": {
								"operands": [
									{
										"expression": {
											"left": "type",
											"operation": "equal",
											"right": "fund"
										}
									},
									{
										"expression": {
											"left": "typespecs",
											"operation": "has_none_of",
											"right": [
												"etf"
											]
										}
									}
								],
								"operator": "and"
							}
						}
					],
					"operator": "or"
				}
			}
		],
		"operator": "and"
	},
	"ignore_unknown_fields": False,
	"markets": [
		"america"
	],
	"options": {
		"lang": "en"
	},
	"range": [
		0,
		100
	],
	"sort": {
		"sortBy": "market_cap_basic",
		"sortOrder": "desc"
	}
}

response = requests.post (
	URL, 
	data = data
)

print (response.text)


