
'''
	https://tvdb.brianthe.dev/
'''


def add_paths_to_system (paths):
	from os.path import dirname, join, normpath
	import pathlib
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'../../modules',
	'../../modules_pip'
])


import rich
from ships.flow.simultaneous import simultaneously

from tradingview_ta import TA_Handler, Interval, Exchange



symbols = [{
	"symbol": "TSLA",
	"screener": "america",
	"exchange": "NASDAQ"
}]


def move (interval):
	tesla = TA_Handler (
		symbol = symbol,
		screener = screener,
		exchange = exchange,
		interval = interval
	)
	
	summary = tesla.get_analysis().summary
	#summary ["interval"] = interval
	#summary.update({ "interval": interval })
	
	'''
	summary = {
		"interval": interval,
		** summary
	}
	'''
	
	'''
	summary = {
		"interval": interval,
		"technicals": str (summary ["BUY"]) + "," + str (summary ["NEUTRAL"]) + "," + str (summary ["SELL"])
	}
	'''
	
	summary = {
		"interval": interval,
		"technicals": summary ["BUY"] - summary ["SELL"]
	}
	
	return summary

'''
	{
		"TSLA": {
			"interval": "1m",
			"technicals": "16,10,0"
		}
	}
'''
data = simultaneously (
	items =  [
		Interval.INTERVAL_1_MINUTE,
		Interval.INTERVAL_5_MINUTES,
		Interval.INTERVAL_15_MINUTES,
		Interval.INTERVAL_1_HOUR,
		Interval.INTERVAL_2_HOURS,
		Interval.INTERVAL_4_HOURS,
		Interval.INTERVAL_1_DAY,
		Interval.INTERVAL_1_DAY,
		Interval.INTERVAL_1_WEEK,
		Interval.INTERVAL_1_MONTH
	],
	capacity = 4,
	move = move
)

rich.print_json (data = data)

#proceeds.insert (0, ["New Name", 25, "Male"])

from tabulate import tabulate
#print (tabulate (table_data, headers="firstrow", tablefmt="grid"))

'''
	
				company
	
	interval			1m			5m		15m
				TSLA	16,10,0		14,9,3	11,10,5
				TSLA	16,10,0		14,9,3	11,10,5
'''

# Transpose the data to make headers horizontal
transposed_data = [[d[key] for d in data] for key in data[0].keys()]

print (transposed_data)

# Get the headers
headers = list(data[0].keys())

print (tabulate(transposed_data, headers=headers, tablefmt="grid"))
