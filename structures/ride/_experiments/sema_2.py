


def add_paths_to_system (paths):
	from os.path import dirname, join, normpath
	import pathlib
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'../../modules',
	'../../modules_pip'
])



import threading
from queue import Queue
import time

# Define a function to process each item and return a result
def process_item (item):
	print ("starting", item)

	time.sleep (item)
	# Simulate some processing
	result = f"Processed item: {item}"
	
	print (result)
	
	return result

# Define a list of items to process
items = [ 3.5, 1, 2, 3 ]

# Define a semaphore with a maximum of 2 permits
semaphore = threading.Semaphore(2)

# Define a function to process items with semaphore limit and return results
def process_with_semaphore(item, results_queue):
	with semaphore:
		result = process_item(item)
		results_queue.put(result)  # Put the result in the queue

# Create a queue to collect the results
results_queue = Queue()

# Create threads to process items
threads = []
for item in items:
	thread = threading.Thread(target=process_with_semaphore, args=(item, results_queue))
	thread.start()
	threads.append(thread)

# Wait for all threads to complete
for thread in threads:
	thread.join()

# Collect results from the queue
results = []
while not results_queue.empty():
	result = results_queue.get()
	results.append(result)

print("All results:", results)
