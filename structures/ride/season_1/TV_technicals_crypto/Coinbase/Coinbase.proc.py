





def add_paths_to_system (paths):
	from os.path import dirname, join, normpath
	import pathlib
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'../../../../modules',
	'../../../../modules_pip'
])


'''
	https://www.coingecko.com/en/categories/layer-1
'''

'''
	
'''

print ("?")

def symbol (the_symbol, description = ""):
	return {
		"symbol": the_symbol,
		"screener": "crypto",
		"exchange": "COINBASE",
		"description": description
	}

import rich
import rollercoaster.clouds.TradingView.treasure.technicals as TV_treasure_tech
symbols_indicators = TV_treasure_tech.scan_symbols (
	capacity = 4,
	symbols = [
		symbol ("00USD"),
		
		#
		#	Decentralized Exchanged:
		#		https://alchemypay.org/
		#
		symbol ("ACHUSD", "FIAT payments"),
		
		symbol ("AEROUSD"),

		
		#
		#	MultiChain L2?
		#
		symbol ("BOBAUSD"),
		symbol ("DEXTUSD"),		
		
		#
		#	
		#	https://district0x.io/
		#
		symbol ("DNTUSD"),
		
		
		#
		#	Ethereum L2:
		#		https://ethernity.io/
		#	
		symbol ("ERNUSD", "Eth L2"),
		
		
		symbol ("FETUSD"),
		symbol ("FIDAUSD"),
		symbol ("FLRUSD"),
		symbol ("GRTUSD"),
		
		#
		#	Stable Coin
		#
		#symbol ("GUSDUSD"),
		
		symbol ("GYENUSD"),
		symbol ("INDEXUSD"),
		
		
		symbol ("MATHUSD", "Wallet"),
		
		#
		#	Solana L2?
		#		https://coinmarketcap.com/currencies/media-network/
		#
		symbol ("MEDIAUSD", "Sol L2"),
		
		#
		#
		#
		symbol ("MDTUSD"),
		symbol ("MKRUSD"),
		
		#
		#	https://marinade.finance/
		#
		symbol ("MSOLUSD", "Sol Stake Maxer"),
		
		#
		#	Muse DAO:
		#		https://docs.musedao.io/
		#
		symbol ("MUSEUSD", "Muse DAO"),
		
		#
		#	Eth L2:
		#		https://orion.xyz/
		#
		symbol ("ORNUSD", "Eth L2"),
		
		#
		#	Eth L2?
		#		SuperRare
		#
		symbol ("RAREUSD", "Eth L2, SuperRare"),
		
		#
		#	https://rendernetwork.com/
		#
		symbol ("RENDERUSD", "GPU Leasing"),
		
		
		symbol ("RNDRUSD"),
		symbol ("SEIUSD", "L1?"),
		symbol ("SHPINGUSD"),
		
		#
		#	
		#
		symbol ("SPAUSD"),
		
		
		symbol ("TIAUSD"),
		
		#
		#	Eth L2?
		#		https://coinmarketcap.com/currencies/chrono-tech/
		#
		symbol ("TIMEUSD", "Eth L2"),
		
		symbol ("VOXELUSD"),
		symbol ("XCNUSD"),

		#
		#	Eth L2: Decentralized Exchange Infrastructure
		#
		symbol ("ZRXUSD", "Eth L2: DeFi Ex")
	]
)

rich.print_json (data = symbols_indicators)

TV_treasure_tech.print_symbols_table (symbols_indicators)