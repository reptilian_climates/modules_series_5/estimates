
'''
	
'''

def add_paths_to_system (paths):
	from os.path import dirname, join, normpath
	import pathlib
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'../../../../modules',
	'../../../../modules_pip'
])


import rich
import rollercoaster.clouds.TradingView.treasure.technicals as TV_treasure_tech
symbols_indicators = TV_treasure_tech.scan_symbols (
	symbols = [{
		"symbol": "DAL",
		"screener": "america",
		"exchange": "NYSE",
		
		"description": "Delta"
	},{
		"symbol": "RYAAY",
		"screener": "america",
		"exchange": "NYSE",
		
		"description": "Ryanair"
	},{
		"symbol": "LUV",
		"screener": "america",
		"exchange": "NYSE",
		
		"description": "Southwest"
	},{
		"symbol": "UAL",
		"screener": "america",
		"exchange": "NASDAQ",
		
		"description": "United"
	},{
		"symbol": "AAL",
		"screener": "america",
		"exchange": "NASDAQ",
		
		"description": "American"
	},{
		"symbol": "ALK",
		"screener": "america",
		"exchange": "NYSE",
		
		"description": "Alaska"
	},{
		"symbol": "CPA",
		"screener": "america",
		"exchange": "NYSE",
		
		"description": "Copa"
	},{
		"symbol": "JBLU",
		"screener": "america",
		"exchange": "NASDAQ",
		
		"description": "Jet Blue"
	},{
		"symbol": "ALGT",
		"screener": "america",
		"exchange": "NASDAQ",
		
		"description": "Allegiant"
	}]
)

rich.print_json (data = symbols_indicators)

TV_treasure_tech.print_symbols_table (symbols_indicators)