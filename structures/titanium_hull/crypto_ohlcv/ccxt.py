

def add_paths_to_system (paths):
	import pathlib
	from os.path import dirname, join, normpath
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()	
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'../../modules',
	'../../modules_pip'
])


import ccxt
import ships.flow.demux_mux2 as demux_mux2

from pprint import pprint
import datetime

print('CCXT Version:', ccxt.__version__)



print(ccxt.exchanges)


exchange = ccxt.kucoin ()

'''
	https://stackoverflow.com/questions/73975532/ccxt-fetch-ohlcv-function
'''
'''
	[
		[
			1504541580000, // UTC timestamp in milliseconds, integer
			4235.4,        // (O)pen price, float
			4240.6,        // (H)ighest price, float
			4230.0,        // (L)owest price, float
			4230.7,        // (C)losing price, float
			37.72941911    // (V)olume float (usually in terms of the base currency, the exchanges docstring may list whether quote or base units are used)
		]
	]
'''
def course (investments):
	print ("investments:", investments)

	timestamp = investments ["timestamp"]
	response = exchange.fetch_ohlcv (
		'BTC/USDT', 
		'15m', 
		timestamp
	)
	
	pprint (response)
	
	return response [0]


proceeds_statement = demux_mux2.start (
	course, 
	[{
		"timestamp": int (
			datetime.datetime.strptime (
				"2023-12-28 03:00:00+00:00", 
				"%Y-%m-%d %H:%M:%S%z"
			).timestamp () * 1000
		)
	},{
		"timestamp": int (
			datetime.datetime.strptime(
				"2023-12-28 03:15:00+00:00", 
				"%Y-%m-%d %H:%M:%S%z"
			).timestamp () * 1000
		)
	}]
)

print ("proceeds_statement:", proceeds_statement)




