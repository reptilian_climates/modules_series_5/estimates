

def add_paths_to_system (paths):
	import pathlib
	from os.path import dirname, join, normpath
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()	
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'../../modules',
	'../../modules_pip'
])


import ccxt
import ships.flow.demux_mux2 as demux_mux2
from pprint import pprint
import datetime

print ('CCXT Version:', ccxt.__version__)


print (ccxt.exchanges)

exchange = ccxt.kucoin ()

'''
	https://stackoverflow.com/questions/73975532/ccxt-fetch-ohlcv-function
'''
'''
	[
		[
			1504541580000, // UTC timestamp in milliseconds, integer
			4235.4,        // (O)pen price, float
			4240.6,        // (H)ighest price, float
			4230.0,        // (L)owest price, float
			4230.7,        // (C)losing price, float
			37.72941911    // (V)olume float (usually in terms of the base currency, the exchanges docstring may list whether quote or base units are used)
		]
	]
'''
'''
	agenda:
		[{
			"high": 30,
			"low": 25,
			"close": 26,
			"open": 28,
			"volume": 1239
		}]
'''

import arrow


'''
	Pacific
'''
def course ():
	timestamp = int (
		datetime.datetime.strptime (
			"2023-12-25 20:00:00+00:00", 
			"%Y-%m-%d %H:%M:%S%z"
		).timestamp () * 1000
	)
	
	response = exchange.fetch_ohlcv (
		'BTC/USDT', 
		'15m', 
		timestamp,
		limit = 1000
	)
	
	
	
	pprint (response)

	parsed = []
	for interval in response:	
		UTC_date_string = arrow.get (interval [0]).datetime.isoformat ();
	
		parsed.append ({
			"UTC timestamp": interval [0],
			"UTC date string": UTC_date_string,
			
			"open": interval [1],
			"high": interval [2],
			"low": interval [3],
			"close": interval [4],
			"volume": interval [5]
		})

	return parsed


intervals = course ()

from rich import print_json
print_json (data = intervals)

print ("intervals count:", len (intervals))



import plotly.graph_objects as go
import pandas as pd
from datetime import datetime

df = pd.DataFrame.from_dict (intervals)

fig = go.Figure (
	data = [
		go.Candlestick (
			x = df ['UTC date string'],
			
			open = df ['open'],
			high = df ['high'],
			low = df ['low'],
			close = df ['close']
		)
	]
)

fig.show ()


