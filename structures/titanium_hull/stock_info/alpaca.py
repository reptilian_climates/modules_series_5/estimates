



def add_paths_to_system (paths):
	import pathlib
	from os.path import dirname, join, normpath
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()	
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'../../modules',
	'../../modules_pip'
])

import datetime
from alpaca.data.timeframe import TimeFrame
import rollercoaster.clouds.Alpaca.shares.structure_1 as structure_1
shares = structure_1.calculate (
	span = [
		datetime.datetime (2023, 11, 28),
		datetime.datetime (2023, 12, 2)
	],
	interval = TimeFrame.Day,
	symbol_or_symbols = [ "BTC/USD" ]
)

import botany
botany.show ({
	"shares": shares
})


import rollercoaster.rides.season_1.TR as TR_indicator
TR_indicator.calc (
	places = shares
)

botany.show ({
	"shares": shares
})