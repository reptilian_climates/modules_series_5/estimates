

'''
	https://api.coinbase.com/api/v3/brokerage/products
	https://api.coinbase.com/api/v3/brokerage/products/{product_id}/candles
'''



def add_paths_to_system (paths):
	import pathlib
	from os.path import dirname, join, normpath
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()	
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'../../modules',
	'../../modules_pip'
])

import json
import rich
fp = open ("/online ellipsis/pertinent/Coinbase.com/coinbase_cloud_api_key_600.JSON", "r")
ellipsis = json.loads (fp.read ())
fp.close ()



from datetime import datetime, timezone
'''
	iso_string = now_utc.isoformat ()
'''

UTC_0_timestamp = datetime.fromisoformat ("2024-01-04T12:34:56.789Z").timestamp ()

from operator import itemgetter
import rollercoaster.clouds.Coinbase.API.products.candles as Coinbase_API_product_candles
candles = Coinbase_API_product_candles.proposal (
	key_name = ellipsis ["name"],
	key_secret = ellipsis ["privateKey"],
	
	product_id = "FET-USD",
	granularity = "FIFTEEN_MINUTE",
	
	start = int (datetime.fromisoformat ("2024-01-04T00:00:00.000Z").timestamp ()),
	end = int (datetime.utcnow ().replace (tzinfo = timezone.utc).timestamp ())
)



