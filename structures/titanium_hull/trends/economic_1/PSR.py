






def add_paths_to_system (paths):
	import pathlib
	from os.path import dirname, join, normpath
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()	
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'../../../modules',
	'../../../modules_pip'
])

palette = {
	"jade": "rgb(0,255,0)",
	"opal": "rgb(0,0,255)"
}

import arrow
import ccxt
import datetime
import json
import pandas
from pprint import pprint
import rich

import ships.flow.demux_mux2 as demux_mux2

print ('CCXT Version:', ccxt.__version__)
print (ccxt.exchanges)

fp = open ("/online ellipsis/cortex/pertinent/Coinbase.com/coinbase_cloud_api_key_600.JSON", "r")
ellipsis = json.loads (fp.read ())
fp.close ()

exchange = ccxt.kraken ()
symbol = "BTC/USDT"

import essentials
jumps = essentials.retrieve_stats (
	symbol = symbol,
	exchange = exchange,
	
	limit = 200
)

import rollercoaster.rides.season_3.physical_span_reversals as PSR
PSR.calc (
	places = jumps,
	spans = 40,
	multiplier = 3
)

rich.print_json (data = jumps)

import pandas
jumps_DF = pandas.DataFrame (jumps)

import rollercoaster.clouds.CCXT.OHLCV.candles as CCXT_OHLCV_candles
chart = CCXT_OHLCV_candles.show (
	DF = jumps_DF
)



'''
import plotly.graph_objects as go
chart.add_trace (
	go.Scatter (
		x = jumps_DF ['UTC date string'], 
		y = jumps_DF ['PS'], 
		showlegend = False
	), 
	row = 2, 
	col = 1
)
'''

def plot_2_venture (jumps_DF_1, venture, color = "white"):
	import plotly.graph_objects as go
	chart.add_trace (
		go.Scatter (
			x = jumps_DF_1 ['UTC date string'], 
			y = jumps_DF_1 [venture], 
			line = dict (
				color = color, 
				width = 3
			)
		),
		row = 2,
		col = 1
	)
	

def plot_1_venture (jumps_DF_1, venture, color = "white"):
	import plotly.graph_objects as go
	chart.add_trace (
		go.Scatter (
			x = jumps_DF_1 ['UTC date string'], 
			y = jumps_DF_1 [venture], 
			line = dict (
				color = color, 
				width = 3
			)
		),
		row = 1,
		col = 1
	)

plot_2_venture (jumps_DF, "PS", color = palette ["jade"])	
plot_2_venture (jumps_DF, "PSA", color = palette ["opal"])	

plot_1_venture (jumps_DF, "PSR UB", color = palette ["opal"])	
plot_1_venture (jumps_DF, "PSR LB", color = palette ["jade"])	
plot_1_venture (jumps_DF, "PSR", color = "rgb(255,100,100)")	

chart.show ()






#