














def add_paths_to_system (paths):
	import pathlib
	from os.path import dirname, join, normpath
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()	
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'../../../modules',
	'../../../modules_pip'
])


def add_shapes_and_annotations (
	jumps_DF,
	ST_direction,
	chart
):
	annotations = []
	shapes = []

	amounts = []

	previous = None
	for index, row in jumps_DF.iterrows ():
		direction = row [ ST_direction ]
		UTC_date_string = row ['UTC date string']
		if (previous in [ -1, 1 ] and direction != previous):		
			if (previous == -1):
				text = '+'
			else:
				text = '-'
				
			amounts.append ([ 
				previous * -1,
				row ["close"]
			])
			
			print (
				"change of direction:", 
				previous * -1, 
				row ['UTC date string'], 
				row ["close"]
			)
			
			shapes.append (
				dict (
					x0 = row ['UTC date string'], 
					x1 = row ['UTC date string'],
					
					y0 = 0, 
					y1 = 1, 
					
					xref = 'x', 
					yref = 'paper',
					
					line_width = 2
				)
			)

			annotations.append (
				dict (
					x = row ['UTC date string'], 
					y = 0.05, 
					
					xref = 'x', 
					yref = 'paper',
					xanchor = 'left',
					
					showarrow = False, 
					 
					text = text
				)
			)
			
			previous = direction;
		
		else:
			previous = direction;

		chart.update_layout (
			shapes = shapes,
			annotations = annotations
		)


def ST ():

	palette = {
		"jade": "rgb(0,255,0)",
		"opal": "rgb(0,0,255)"
	}

	import arrow
	import ccxt
	import datetime
	import json
	import pandas
	from pprint import pprint
	import rich

	import ships.flow.demux_mux2 as demux_mux2

	print ('CCXT Version:', ccxt.__version__)
	print (ccxt.exchanges)

	fp = open ("/online ellipsis/cortex/pertinent/Coinbase.com/coinbase_cloud_api_key_600.JSON", "r")
	ellipsis = json.loads (fp.read ())
	fp.close ()

	exchange = ccxt.kraken ()
	symbol = "BTC/USDT"

	import essentials
	jumps = essentials.retrieve_stats (
		symbol = symbol,
		exchange = exchange,
		
		limit = 200
	)




	

	# Example usage:
	# Assuming you have a list of dictionaries 'data_list' with keys 'high', 'low', and 'close'
	# data_list = [...]

	# Calculate SuperTrend with default period of 10 and multiplier of 3.0, and modify the input list
	#calculate_and_add_supertrend(jumps)


	import rich
	rich.print_json (data = jumps)

	import pandas
	jumps_DF = pandas.DataFrame (jumps)

	'''
	
	'''
	import pandas_ta as ta
	ST_length = 7
	ST_multiplier = 3.0
	ST_direction = f"SUPERTd_{ str (ST_length) }_{ str (ST_multiplier) }"
	sti = ta.supertrend(jumps_DF['high'], jumps_DF['low'], jumps_DF['close'], length=ST_length, multiplier=ST_multiplier)
	jumps_DF = jumps_DF.join (sti)




	import rollercoaster.clouds.CCXT.OHLCV.candles as CCXT_OHLCV_candles
	chart = CCXT_OHLCV_candles.show (
		DF = jumps_DF
	)

	def plot_2_venture (jumps_DF_1, venture, color = "white"):
		import plotly.graph_objects as go
		chart.add_trace (
			go.Scatter (
				x = jumps_DF_1 ['UTC date string'], 
				y = jumps_DF_1 [venture], 
				line = dict (
					color = color, 
					width = 3
				)
			),
			row = 2,
			col = 1
		)
		

	def plot_1_venture (jumps_DF_1, venture, color = "white"):
		import plotly.graph_objects as go
		chart.add_trace (
			go.Scatter (
				x = jumps_DF_1 ['UTC date string'], 
				y = jumps_DF_1 [venture], 
				line = dict (
					color = color, 
					width = 3
				)
			),
			row = 1,
			col = 1
		)

	#plot_2_venture (jumps_DF, "PS", color = palette ["jade"])	
	#plot_2_venture (jumps_DF, "PSA", color = palette ["opal"])	

	#plot_1_venture (jumps_DF, "PSR UB", color = palette ["opal"])	
	#plot_1_venture (jumps_DF, "PSR LB", color = palette ["jade"])	
	plot_1_venture (jumps_DF, "SUPERT_7_3.0", color = "rgb(255,100,100)")	

	rich.print_json (data = jumps_DF.to_dict ('records'))


	

	add_shapes_and_annotations (
		jumps_DF,
		ST_direction,
		chart
	)

	'''
	import essentials
	essentials.calculate_win_rate (amounts);
	'''

	chart.show ()
	
ST ();


