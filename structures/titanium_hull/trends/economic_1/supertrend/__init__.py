

def calc (data_list, period=10, multiplier=3.0):
	atr_values = []

	for i in range(1, len(data_list)):
		high_low = data_list[i]['high'] - data_list[i]['low']
		high_prev_close = abs(data_list[i]['high'] - data_list[i - 1]['close'])
		low_prev_close = abs(data_list[i]['low'] - data_list[i - 1]['close'])

		tr = max(high_low, high_prev_close, low_prev_close)
		atr_values.append(tr)

	atr = [sum(atr_values[:period]) / period]

	for i in range(period, len(atr_values)):
		atr.append((atr[-1] * (period - 1) + atr_values[i]) / period)

	data_list[period - 1]['atr'] = atr[-1]

	for i in range(period, len(data_list)):
		data_list[i]['atr'] = ((data_list[i - 1]['atr'] * (period - 1)) + atr_values[i - 1]) / period

		data_list[i]['upper_band'] = data_list[i]['high'] + multiplier * data_list[i]['atr']
		data_list[i]['lower_band'] = data_list[i]['low'] - multiplier * data_list[i]['atr']

		if data_list[i - 1]['close'] > data_list[i - 1]['upper_band']:
			data_list[i]['trend'] = 'Down'
			data_list[i]['supertrend'] = data_list[i]['upper_band']
		else:
			data_list[i]['trend'] = 'Up'
			data_list[i]['supertrend'] = data_list[i]['lower_band']