






def add_paths_to_system (paths):
	import pathlib
	from os.path import dirname, join, normpath
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()	
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'../../../modules',
	'../../../modules_pip'
])


import arrow
import ccxt
import datetime
import json
import pandas
from pprint import pprint
import rich

import ships.flow.demux_mux2 as demux_mux2

print ('CCXT Version:', ccxt.__version__)
print (ccxt.exchanges)

fp = open ("/online ellipsis/cortex/pertinent/Coinbase.com/coinbase_cloud_api_key_600.JSON", "r")
ellipsis = json.loads (fp.read ())
fp.close ()


exchange = ccxt.kraken ()
symbol = "BTC/USDT"


import essentials
jumps = essentials.retrieve_stats (
	symbol = symbol,
	exchange = ccxt.kraken ()
)

import rollercoaster.rides.season_3.VSA as VSA_tap
import rollercoaster.rides.season_3.VDA as VDA_tap

VSA_tap.calc (jumps)
VDA_tap.calc (jumps)

import pandas
jumps_DF = pandas.DataFrame (jumps)

print (jumps_DF)

import rollercoaster.clouds.CCXT.OHLCV.candles as CCXT_OHLCV_candles
chart = CCXT_OHLCV_candles.show (
	DF = jumps_DF
)

def plot_venture (jumps_DF_1, venture, color = "white"):
	import plotly.graph_objects as go
	chart.add_trace (
		go.Scatter (
			x = jumps_DF_1 ['UTC date string'], 
			y = jumps_DF_1 [venture], 
			line = dict (
				color = color, 
				width = 1
			)
		),
		row = 1,
		col = 1
	)
	

	
chart.layout.annotations[0].update (text = symbol)

#plot_LB (jumps_DF)
#plot_UB (jumps_DF)

plot_venture (jumps_DF, "VDA", color = "lime")
plot_venture (jumps_DF, "VSA", color = "purple")

chart.show ()






#