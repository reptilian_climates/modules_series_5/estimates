
def add_paths_to_system (paths):
	import pathlib
	from os.path import dirname, join, normpath
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()	
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'../../../modules',
	'../../../modules_pip'
])

palette = {
	"jade": "rgb(0,255,0)",
	"opal": "rgb(0,0,255)"
}

import numpy as np
import yfinance as yf
import pandas_datareader as pdr
import pandas as pd

def retrieve_ellipsis ():
	import json
	fp = open ("/offline/cortex/basal_ganglia/Coinbase.com/coinbase_cloud_api_Trading-Awareness-Key-200.JSON", "r")
	ellipsis = json.loads (fp.read ())
	fp.close ()
	
	return ellipsis;


'''
	calendar:
	
		[ ] {
				start: 1 day ago (24 hours ago)
				end: now
			}
				15 -> 4 * 24 = 96
'''

'''
	15 minutes -> 4 * 24 = 96
		96 * 3 = 288
'''
def retrieve_Coinbase_trading_data (
	ellipsis
):
	#product_id = "FET-USD"
	#product_id = "TIA-USD"
	#product_id = "BTC-USD"
	product_id = "ETH-USD"

	
	
	from datetime import datetime, timezone, timedelta
	#UTC_0_timestamp = datetime.fromisoformat ("2024-01-04T12:34:56.789Z").timestamp ()
	
	'''
	granularity = "FIFTEEN_MINUTE"
	start = int (
		(
			datetime.utcnow ().replace (tzinfo = timezone.utc) - timedelta (days = 9)
		).timestamp ()
	)
	end = int (datetime.utcnow ().replace (tzinfo = timezone.utc).timestamp ())
	'''
	
	granularity = "ONE_HOUR"
	start = int (
		(
			datetime.utcnow ().replace (tzinfo = timezone.utc) - timedelta (days = 12)
		).timestamp ()
	)
	end = int (datetime.utcnow ().replace (tzinfo = timezone.utc).timestamp ())

	print ('start:', start)
	print ('end:', end)
	

	from operator import itemgetter
	import rollercoaster.clouds.Coinbase.API.products.candles as Coinbase_API_product_candles
	OHLCV_DF = Coinbase_API_product_candles.proposal (
		key_name = ellipsis ["name"],
		key_secret = ellipsis ["privateKey"],
		
		product_id = product_id,
		granularity = granularity,
		
		start = start,
		end = end
	)
	
	#import pandas
	#OHLCV_DF_DF = pandas.DataFrame (OHLCV_DF)
	
	return OHLCV_DF


	
def build_chart (OHLCV_DF):
	import rollercoaster.clouds.CCXT.OHLCV.candles as CCXT_OHLCV_candles
	chart = CCXT_OHLCV_candles.show (
		DF = OHLCV_DF
	)
	
	return chart;


def plot_1_venture (chart, OHLCV_DF, venture, color = "white"):
	import plotly.graph_objects as go
	chart.add_trace (
		go.Scatter (
			x = OHLCV_DF ['UTC date string'], 
			y = OHLCV_DF [ venture ], 
			line = dict (
				color = color, 
				width = 3
			)
		),
		row = 1,
		col = 1
	)
	
def plot_1_buy_and_sell (chart, OHLCV_DF):
	import numpy
	import plotly.graph_objects as go
	chart.add_trace (
		go.Scatter (
			x = OHLCV_DF ['UTC date string'],
			y = OHLCV_DF ["close"],
			
			marker_color = numpy.select (
				[
					OHLCV_DF [ "ST_BUY_SELL" ] == "SELL", 
					OHLCV_DF [ "ST_BUY_SELL" ] == "BUY"
				], 
				["orange", "purple"], 
				"rgba(0,0,0,0)"
			),
			
			mode = "markers",
			#marker_color = "black",
			yaxis = "y2",
			name = "Bubble"
		),
		row = 1, 
		col = 1
	)


def main_2 ():
	DF = retrieve_Coinbase_trading_data (
		ellipsis = retrieve_ellipsis ()
	)	
	print (DF)
	
	import rollercoaster.rides.season_3.super_hero_trend as super_hero_trend
	DF = super_hero_trend.calc (
		DF,
		period = 14,
		multiplier = 3
	)
	
	chart = build_chart (DF)
	plot_1_venture (chart, DF, "ST", color = palette ["opal"])
	plot_1_buy_and_sell (chart, DF)
	
	#calculate_win_rate (DF)
	
	import rollercoaster.rides.season_3.super_hero_trend.win_rates.shares_trading as SHT_WR_shares_trading
	SHT_WR_shares_trading.calc (DF)
	
	chart.show ()
	
	#print (DF.at [0, "close"])
	
	print (DF ["close"].iloc [0])
	print (DF ["close"].iloc [-1])
	
	
	
main_2 ();