
def add_paths_to_system (paths):
	import pathlib
	from os.path import dirname, join, normpath
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()	
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'../../../modules',
	'../../../modules_pip'
])

palette = {
	"jade": "rgb(0,255,0)",
	"opal": "rgb(0,0,255)"
}

import numpy as np
import yfinance as yf
import pandas_datareader as pdr
import pandas as pd

def retrieve_ellipsis ():
	import json
	fp = open ("/offline/cortex/basal_ganglia/Coinbase.com/coinbase_cloud_api_Trading-Awareness-Key-200.JSON", "r")
	ellipsis = json.loads (fp.read ())
	fp.close ()
	
	return ellipsis;


'''
	calendar:
	
		[ ] {
				start: 1 day ago (24 hours ago)
				end: now
			}
				15 -> 4 * 24 = 96
'''

'''
	15 minutes -> 4 * 24 = 96
		96 * 3 = 288
'''
def retrieve_Coinbase_trading_data (
	ellipsis
):
	#product_id = "FET-USD"
	product_id = "TIA-USD"
	#product_id = "BTC-USD"

	
	
	from datetime import datetime, timezone, timedelta
	#UTC_0_timestamp = datetime.fromisoformat ("2024-01-04T12:34:56.789Z").timestamp ()
	
	'''
	granularity = "FIFTEEN_MINUTE"
	start = int (
		(
			datetime.utcnow ().replace (tzinfo = timezone.utc) - timedelta (days = 9)
		).timestamp ()
	)
	end = int (datetime.utcnow ().replace (tzinfo = timezone.utc).timestamp ())
	'''
	
	granularity = "ONE_HOUR"
	start = int (
		(
			datetime.utcnow ().replace (tzinfo = timezone.utc) - timedelta (days = 12)
		).timestamp ()
	)
	end = int (datetime.utcnow ().replace (tzinfo = timezone.utc).timestamp ())

	print ('start:', start)
	print ('end:', end)
	

	from operator import itemgetter
	import rollercoaster.clouds.Coinbase.API.products.candles as Coinbase_API_product_candles
	OHLCV_DF = Coinbase_API_product_candles.proposal (
		key_name = ellipsis ["name"],
		key_secret = ellipsis ["privateKey"],
		
		product_id = product_id,
		granularity = granularity,
		
		start = start,
		end = end
	)
	
	#import pandas
	#OHLCV_DF_DF = pandas.DataFrame (OHLCV_DF)
	
	return OHLCV_DF

def main ():
	'''
		data retrieved                                   Open         High          Low        Close    Adj Close  Volume
		Datetime                                                                                          
		2023-12-13 09:15:00+05:30  2181.000000  2185.000000  2172.100098  2174.949951  2174.949951       0
		2023-12-13 09:20:00+05:30  2174.550049  2187.949951  2173.000000  2184.149902  2184.149902   14934
		2023-12-13 09:25:00+05:30  2183.250000  2187.000000  2177.100098  2177.550049  2177.550049   10632
		2023-12-13 09:30:00+05:30  2179.000000  2184.550049  2176.149902  2176.149902  2176.149902    8707
		2023-12-13 09:35:00+05:30  2176.199951  2181.500000  2172.350098  2180.300049  2180.300049    8408
		...                                ...          ...          ...          ...          ...     ...
		2024-01-12 15:05:00+05:30  2331.750000  2334.600098  2327.000000  2332.550049  2332.550049   16110
		2024-01-12 15:10:00+05:30  2334.050049  2334.050049  2331.149902  2332.449951  2332.449951    6363
		2024-01-12 15:15:00+05:30  2332.449951  2334.399902  2330.550049  2330.550049  2330.550049    5300
		2024-01-12 15:20:00+05:30  2330.550049  2334.600098  2330.300049  2334.050049  2334.050049    6289
		2024-01-12 15:25:00+05:30  2334.500000  2337.000000  2330.000000  2330.000000  2330.000000    6465

		[1650 rows x 6 columns]
		data retrieved              open         high          low        close    Adj Close  Volume
		0     2181.000000  2185.000000  2172.100098  2174.949951  2174.949951       0
		1     2174.550049  2187.949951  2173.000000  2184.149902  2184.149902   14934
		2     2183.250000  2187.000000  2177.100098  2177.550049  2177.550049   10632
		3     2179.000000  2184.550049  2176.149902  2176.149902  2176.149902    8707
		4     2176.199951  2181.500000  2172.350098  2180.300049  2180.300049    8408
		...           ...          ...          ...          ...          ...     ...
		1645  2331.750000  2334.600098  2327.000000  2332.550049  2332.550049   16110
		1646  2334.050049  2334.050049  2331.149902  2332.449951  2332.449951    6363
		1647  2332.449951  2334.399902  2330.550049  2330.550049  2330.550049    5300
		1648  2330.550049  2334.600098  2330.300049  2334.050049  2334.050049    6289
		1649  2334.500000  2337.000000  2330.000000  2330.000000  2330.000000    6465
	'''
	'''
	data = yf.download ("ACC.NS", period = "1mo", interval = "5m")
	print ("data retrieved", data)
	
	data=data.reset_index (drop = True)
	data.rename (
		columns = {
			"High": "high", 
			"Low": "low", 
			
			"Close": "close",
			"Open": "open"
		},
		inplace = True
	)
	'''

	'''
		data retrieved           start     low    high    open   close    volume      UTC date string
		0    1705118400  15.781  16.259  15.800  16.175  40072.88  2024-01-13T04:00:00
		1    1705114800  15.421  15.971  15.765  15.803  95651.47  2024-01-13T03:00:00
		2    1705111200  15.744  16.162  16.162  15.772  38099.77  2024-01-13T02:00:00
		3    1705107600  15.483  16.221  16.152  16.114  97314.57  2024-01-13T01:00:00
		4    1705104000  16.082  16.699  16.407  16.166  90440.50  2024-01-13T00:00:00
		..          ...     ...     ...     ...     ...       ...                  ...
		283  1704099600  11.963  12.155  12.037  12.141   8170.91  2024-01-01T09:00:00
		284  1704096000  11.944  12.083  12.032  12.039  10566.59  2024-01-01T08:00:00
		285  1704092400  11.951  12.078  11.957  12.035  19061.94  2024-01-01T07:00:00
		286  1704088800  11.743  11.967  11.763  11.952  15980.00  2024-01-01T06:00:00
		287  1704085200  11.738  11.879  11.833  11.762  13943.39  2024-01-01T05:00:00
	'''
	data = retrieve_Coinbase_trading_data (
		ellipsis = retrieve_ellipsis ()
	)
	
	print ("data retrieved", data)
	
	# reverse the DF
	#data = data.iloc[::-1]
	data = data [::-1].reset_index(drop = True)

	print ("data retrieved reversed", data)

	data['tr0'] = abs(data["high"] - data["low"])
	data['tr1'] = abs(data["high"] - data["close"].shift(1))
	data['tr2'] = abs(data["low"]- data["close"].shift(1))
	data["TR"] = round(data[['tr0', 'tr1', 'tr2']].max(axis=1),2)
	data["ATR"]=0.00
	data['BUB']=0.00
	data["BLB"]=0.00
	data["FUB"]=0.00
	data["FLB"]=0.00
	data["ST"]=0.00

	# Calculating ATR 
	for i, row in data.iterrows():
		if i == 0:
			data.loc[i,'ATR'] = 0.00#data['ATR'].iat[0]
		else:
			data.loc[i,'ATR'] = ((data.loc[i-1,'ATR'] * 13)+data.loc[i,'TR'])/14

	data['BUB'] = round(((data["high"] + data["low"]) / 2) + (2 * data["ATR"]),2)
	data['BLB'] = round(((data["high"] + data["low"]) / 2) - (2 * data["ATR"]),2)


	# FINAL UPPERBAND = IF( (Current BASICUPPERBAND < Previous FINAL UPPERBAND) or (Previous close > Previous FINAL UPPERBAND))
	#                     THEN (Current BASIC UPPERBAND) ELSE Previous FINALUPPERBAND)


	for i, row in data.iterrows():
		if i==0:
			data.loc[i,"FUB"]=0.00
		else:
			if (data.loc[i,"BUB"]<data.loc[i-1,"FUB"])|(data.loc[i-1,"close"]>data.loc[i-1,"FUB"]):
				data.loc[i,"FUB"]=data.loc[i,"BUB"]
			else:
				data.loc[i,"FUB"]=data.loc[i-1,"FUB"]

	# FINAL LOWERBAND = IF( (Current BASIC LOWERBAND > Previous FINAL LOWERBAND) or (Previous close < Previous FINAL LOWERBAND)) 
	#                     THEN (Current BASIC LOWERBAND) ELSE Previous FINAL LOWERBAND)

	for i, row in data.iterrows():
		if i==0:
			data.loc[i,"FLB"]=0.00
		else:
			if (data.loc[i,"BLB"]>data.loc[i-1,"FLB"])|(data.loc[i-1,"close"]<data.loc[i-1,"FLB"]):
				data.loc[i,"FLB"]=data.loc[i,"BLB"]
			else:
				data.loc[i,"FLB"]=data.loc[i-1,"FLB"]



	# SUPERTREND = IF((Previous SUPERTREND = Previous FINAL UPPERBAND) and (Current close <= Current FINAL UPPERBAND)) THEN
	#                 Current FINAL UPPERBAND
	#             ELSE
	#                 IF((Previous SUPERTREND = Previous FINAL UPPERBAND) and (Current close > Current FINAL UPPERBAND)) THEN
	#                     Current FINAL LOWERBAND
	#                 ELSE
	#                     IF((Previous SUPERTREND = Previous FINAL LOWERBAND) and (Current close >= Current FINAL LOWERBAND)) THEN
	#                         Current FINAL LOWERBAND
	#                     ELSE
	#                         IF((Previous SUPERTREND = Previous FINAL LOWERBAND) and (Current close < Current FINAL LOWERBAND)) THEN
	#                             Current FINAL UPPERBAND


	for i, row in data.iterrows():
		if i==0:
			data.loc[i,"ST"]=0.00
		elif (data.loc[i-1,"ST"]==data.loc[i-1,"FUB"]) & (data.loc[i,"close"]<=data.loc[i,"FUB"]):
			data.loc[i,"ST"]=data.loc[i,"FUB"]
		elif (data.loc[i-1,"ST"]==data.loc[i-1,"FUB"])&(data.loc[i,"close"]>data.loc[i,"FUB"]):
			data.loc[i,"ST"]=data.loc[i,"FLB"]
		elif (data.loc[i-1,"ST"]==data.loc[i-1,"FLB"])&(data.loc[i,"close"]>=data.loc[i,"FLB"]):
			data.loc[i,"ST"]=data.loc[i,"FLB"]
		elif (data.loc[i-1,"ST"]==data.loc[i-1,"FLB"])&(data.loc[i,"close"]<data.loc[i,"FLB"]):
			data.loc[i,"ST"]=data.loc[i,"FUB"]

	# Buy Sell Indicator
	for i, row in data.iterrows():
		if i==0:
			data["ST_BUY_SELL"]="NA"
		elif (data.loc[i,"ST"]<data.loc[i,"close"]) :
			data.loc[i,"ST_BUY_SELL"]="BUY"
		else:
			data.loc[i,"ST_BUY_SELL"]="SELL"
			
	return data;
	
def build_chart (OHLCV_DF):
	import rollercoaster.clouds.CCXT.OHLCV.candles as CCXT_OHLCV_candles
	chart = CCXT_OHLCV_candles.show (
		DF = OHLCV_DF
	)
	
	return chart;


def plot_1_venture (chart, OHLCV_DF, venture, color = "white"):
	import plotly.graph_objects as go
	chart.add_trace (
		go.Scatter (
			x = OHLCV_DF ['UTC date string'], 
			y = OHLCV_DF [ venture ], 
			line = dict (
				color = color, 
				width = 3
			)
		),
		row = 1,
		col = 1
	)

def main_2 ():
	DF = main ()
	print (DF)
	
	chart = build_chart (DF)
	plot_1_venture (chart, DF, "ST", color = palette ["opal"])
	
	chart.show ()
	
main_2 ();