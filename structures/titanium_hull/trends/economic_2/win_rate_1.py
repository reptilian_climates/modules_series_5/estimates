






def add_paths_to_system (paths):
	import pathlib
	from os.path import dirname, join, normpath
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()	
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'../../../modules',
	'../../../modules_pip'
])

symbol = "BTC/USDT"

import ccxt
import ships.flow.demux_mux2 as demux_mux2
from pprint import pprint
import datetime
import rich
import arrow
import json
import ccxt
import pandas
import pandas_ta
import plotly.graph_objects as go


import essentials


def retrieve_ellipsis ():
	fp = open ("/offline/cortex/basal_ganglia/Coinbase.com/coinbase_cloud_api_Trading-Awareness-Key-200.JSON", "r")
	ellipsis = json.loads (fp.read ())
	fp.close ()
	
	return ellipsis;


class data_frame:
	def __init__ (this):
		pass;
		

def win_rate ():
	product_id = "FET-USD"

	def print_ccxt_info (ccxt):
		print ('CCXT Version:', ccxt.__version__)
		print (ccxt.exchanges)
	
	ellipsis = retrieve_ellipsis ()

	#exchange = ccxt.kraken ()

	OHLCV_DF = essentials.retrieve_Coinbase_OHLCV (
		ellipsis = ellipsis,
		product_id = product_id
	)


	import rollercoaster.rides.season_3.ST as ST_tap
	ST = ST_tap.calc (OHLCV_DF, length = 50, multiplier = 1.4)

	#ST = calc_super_trend (OHLCV_DF)
	supertrend_DF = ST.DF;
	ST_direction = ST.direction;

	SMA_10 = pandas_ta.sma (OHLCV_DF ["close"], length = 10)

	OHLCV_DF = OHLCV_DF.join (SMA_10)
	OHLCV_DF = OHLCV_DF.join (supertrend_DF)

	print (OHLCV_DF)


	'''

		Chart:
			This configures the candles figure.
	
	'''
	import rollercoaster.clouds.CCXT.OHLCV.candles as CCXT_OHLCV_candles
	chart = CCXT_OHLCV_candles.show (
		DF = OHLCV_DF
	)

	'''
		SMA 10
	'''
	chart.add_trace (
		go.Scatter (
			x = OHLCV_DF ['UTC date string'], 
			y = OHLCV_DF ["SMA_10"], 
			line = dict (color = 'orange', width = 1)
		),
		row = 1,
		col = 1
	)
	
	ST_tap.plot (
		chart,
		OHLCV_DF,
		ST_direction
	)

	win_rate = 1;

	amounts = []

	print ("row 1", OHLCV_DF.iloc [1])
	print ("rows count", len (OHLCV_DF))


	chart.show ()



win_rate ();